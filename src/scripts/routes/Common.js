// JS components
import TypeFont from '../components/TypeFont';
import Timer from '../components/Timer'

export default {
  init() { // scripts here run on the DOM load event
    TypeFont.init();
    // Run timer
    setInterval( () => Timer.printTime(), 1000);
  },
  finalize() { // scripts here fire after init() runs
  },
};
