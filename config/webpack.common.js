const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  entry: {
    app: './src/app.js',
  },
  output: {
    filename: '[name].bundle.[hash].js',
    chunkFilename: '[name].bundle.[hash].js',
    path: path.resolve(__dirname, '../dist/scripts/'),
  },
  resolve: {
    extensions: ['.js'],
  },
  module: {
    rules : [
      {
        test: /\.(js)$/,
        exclude: /(node_modules)/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  "@babel/preset-env",
                  {
                    "useBuiltIns": "entry"
                  },
                ],
              ],
            }
          }, {
            loader: 'eslint-loader'
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: {
          loader: "url-loader",
          options: {
            limit: 8192,
            name: "[name].[ext]",
            outputPath: '../images/',
            // fallback: 'file-loader',
            // name: "[name].[ext]",
          }
        }
      },
      {
         test: /\.(woff|woff2|eot|ttf|otf)$/,
         use: {
           loader: 'file-loader',
           options: {
              name: '[name].[ext]',
              outputPath: '../fonts/'
            }
         }
      },
      // {
      //   test: /\.(html)$/,
      //   use: {
      //     loader: 'html-loader',
      //   }
      // }
      // {
      //   test: /bootstrap\/dist\/js\/umd\//, use: 'imports-loader?jQuery=jquery'
      // }
    ],
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
    }),
    new CleanWebpackPlugin(['dist']),
  ],
}
