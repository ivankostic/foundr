const merge = require('webpack-merge')
const common = require('./webpack.common.js')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const PurifyCSSPlugin = require('purifycss-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const glob = require('glob-all');

module.exports = merge(common, {
  mode: 'production',
  devtool: 'false',
  module: {
     rules: [
       {
         test: /\.scss$/,
          use: ExtractTextPlugin.extract({
           fallback: 'style-loader',
           use: [
             { loader: 'css-loader', options: { sourceMap: false  } },
             {
               loader: 'postcss-loader',
               options: {
                 sourceMap: false,
                 plugins: [
                   require('autoprefixer')(),
                 ],
               },
             },
             { loader: 'sass-loader', options: { sourceMap: false } },
           ],
         }),
       },
     ],

   },
   plugins: [
     new ExtractTextPlugin({
         filename:  (getPath) => {
           return getPath('../styles/[name].bundle.[hash].css').replace('css/js', 'css');
         },
         allChunks: true
     }),
     new HtmlWebpackPlugin({
       title: '| Foundr',
       'meta': {
         'viewport': 'width=device-width, initial-scale=1, shrink-to-fit=no',
       },
       template: './src/index.html',
       filename: '../index.html',
       minify: {
          collapseWhitespace: true,
          removeComments: true,
          useShortDoctype: true
       }
     }),
      new PurifyCSSPlugin({
        paths: glob.sync([
          path.join(__dirname, '../src/*.html'),
        ]),
        minimize: true,
        purifyOptions: {
          whitelist: [
            '*alignright*',
          ]
        }
     }),
  ],
})
